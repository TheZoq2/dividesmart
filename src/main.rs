use std::collections::HashMap;

use color_eyre::{Result, eyre::Context};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct Config {
    people: HashMap<String, Vec<f64>>,
}

fn main() -> Result<()> {
    let file =
        std::fs::read_to_string("dividesmart.toml").context("Failed to read dividesmart.toml")?;

    let cfg = toml::from_str::<Config>(&file).context("Failed to decode dividesmart.toml")?;

    let total_expenses = cfg.people.values().flatten().sum::<f64>();

    let per_person = total_expenses / cfg.people.len() as f64;
    println!("To pay per person before subtraction: {per_person}");

    for (name, expenses) in cfg.people {
        let total_paid = expenses.iter().sum::<f64>();

        println!("{name}: {}", per_person - total_paid)
    }

    Ok(())
}
